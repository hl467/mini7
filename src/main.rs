use qdrant_client::prelude::*;
use anyhow::Result;
use qdrant_client::qdrant::{
    CreateCollection, PointStruct, SearchPoints, VectorParams, VectorsConfig
};
use qdrant_client::qdrant::vectors_config::Config;
use serde_json::json;
use std::convert::TryInto;

#[tokio::main]
async fn main() -> Result<()> {
    let client = QdrantClient::from_url("http://localhost:6334").build()?;
    let collection_name = "test_collection";
    create_collection(&client, collection_name).await?;
    ingest_data(&client, collection_name).await?;
    search_query(&client, collection_name).await?;

    Ok(())
}


// Function to create a collection in Qdrant
async fn create_collection(client: & QdrantClient, collection_name: &str) -> Result<()> {
    // delete collection if it already exists
    client.delete_collection(&collection_name).await.ok();
    // Create a new collection
    client.create_collection(&CreateCollection {
        collection_name: collection_name.into(),
        vectors_config: Some(VectorsConfig {
            config: Some(Config::Params(VectorParams {
                size: 3,
                distance: Distance::Cosine.into(),
                ..Default::default()
            })),
        }),
        ..Default::default()
    }).await?;

    Ok(())

}

// ingest data into Qdrant
async fn ingest_data(client: & QdrantClient, collection_name: &str) -> Result<()> {
    // Prepare the points to be ingested
    let points = vec![
        PointStruct::new (
            1,
            vec![1.0, 2.0, 3.0],
            json!({"name": "Alice"}).try_into().unwrap(),
        ),
        PointStruct::new (
            2,
            vec![4.0, 5.0, 6.0],
            json!({"name": "Bob"}).try_into().unwrap(),
        ),
        PointStruct::new (
            3,
            vec![7.0, 8.0, 9.0],
            json!({"name": "Charlie"}).try_into().unwrap(),
        ),
        PointStruct::new (
            4,
            vec![10.0, 11.0, 12.0],
            json!({"name": "David"}).try_into().unwrap(),
        ), 
        PointStruct::new (
            5,
            vec![13.0, 14.0, 15.0],
            json!({"name": "Eve"}).try_into().unwrap(),
        ),
    ];

    // Ingest multiple points into the database
    client.upsert_points(collection_name, None, points, None).await?;

    Ok(())
}

// perform a search query in Qdrant
async fn search_query(client: & QdrantClient, collection_name: &str) -> Result<()> {
    // Query multiple points from the database
    let search_result = client
        .search_points(&SearchPoints {
            collection_name: collection_name.into(),
            vector: vec![7.0, 2.0, 1.0],
            limit: 3,
            with_payload: Some(true.into()),
            ..Default::default()
        })
        .await
        .unwrap();
    // Visualize the output for each found point
    for point in search_result.result.iter() {
        println!("Point: {:?}", point);
    }
    Ok(())
}
