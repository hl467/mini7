# Mini7 Project: Data Processing with Vector Database

This project is a simple example of how to use the Qdrant client in Rust dealing with creating a collection, ingesting data into the collection, and performing a search query in the collection. 

## Details


### Create a new collection using Qdrant database
- Create a new collection in Qdrant database from Dockerhub image. 
using the following command:
```bash
docker run -p 6333:6333 -p 6334:6334 \
    -e QDRANT__SERVICE__GRPC_PORT="6334" \
    qdrant/qdrant
```
- The Qdrant server will be running at `http://localhost:6334`.
- screenshot of the running server:
![Qdrant Server](datarun.png)

### Main functions
- `create_collection`: This function creates a new collection in Qdrant. If the collection already exists, it deletes it first.
- `ingest_data`: This function ingests data into the collection. It prepares the points to be ingested and then ingests them into the database.
- `search_query`: This function performs a search query in the collection. It queries multiple points from the database and visualizes the output for each found point.
    - screenshot of the search query:
    ![Search Query](visualization.png)

